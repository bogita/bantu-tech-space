const HtmlWebpackPlugin = require('html-webpack-plugin'); 
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CompressionPlugin = require("compression-webpack-plugin");
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const webpack = require('webpack');
const path = require('path');
const extractCSS = new ExtractTextPlugin('style.css');

module.exports = {
    entry: {
        bundle: [ path.resolve(__dirname, 'src', 'client', 'index.jsx'), 'webpack-dev-server/client?http://localhost:8080']
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js',
        publicPath: '/dist/'
    },
    module: {
        loaders: [
            {
                test: /\.(js|jsx)$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            { test: /\.(ts|tsx)$/, loader: 'ts-loader' },
            {
                test: /\.scss$/,
                loader: extractCSS.extract(['css-loader','sass-loader']),
                exclude: [/node_modules/]
            },
            {
                test: /\.css$/,
                loader:  'style-loader!css-loader',
                include: [/font-awesome/, /slick-carousel/]
            }
      
        ]
    },
    resolve: {
      extensions: ['.ts', '.tsx', '.js', '.jsx']
    },
    plugins: [
        extractCSS,
        new webpack.optimize.UglifyJsPlugin(),
        new HtmlWebpackPlugin({
            template: './src/client/index.html',
            filename: "./index.html"
        }),
        new webpack.DefinePlugin({
          'process.env.BROWSER': JSON.stringify(true)
        }),
        new ExtractTextPlugin({
            filename: '[name].css',
            allChunks: true
        })
    ],
    devtool: 'inline-source-map',
    devServer: {
        historyApiFallback: true,
        port: 9891,
        contentBase:  path.resolve(__dirname, 'dist'),
        compress: true,
        stats: 'errors-only',
        proxy: [
          {
            context: ['/api', '/assets'],
            target: 'http://localhost:9890',
            changeOrigin: false,
            secure: false
          }
        ]
    },
    performance: { hints: false }
};