import React, {Component} from 'react'
import Member from '../../components/member/index'
import TeamMember from '../../components/team-member/index'
import style from './scss/team.scss'
import Data from './team-data'

export default class Team extends Component {
    
    constructor(props){
        super(props)
    }

    render(){
        const members = Data.team.map( (elem, i) => {
            return <TeamMember key={i} {...elem} />
        })

        return(
            <div className="team-wrapper">
                <div className="team">
                    {members}
                </div>
            </div>
        )
    }

}
