export default {
    team: [
        {
            img: "/assets/images/JohnNdungu.jpeg",
            name: "John Ndungu",
            title: "Software Developer",
        },
        {
            img: "/assets/images/bogita.jpg",
            name: "Hiram Njogu",
            title: "Software Developer",
        },
        {
            img: "/assets/images/chris.JPG",
            name: "Christopher Chege",
            title: "Network Engineer",
        },
        {
            img: "/assets/images/muraya.jpg",
            name: "Simon Mwangi ",
            title: "Software Developer",
        },
        {
            img: "/assets/images/leah.jpg",
            name: "Leah Njoroge",
            title: "Software Developer",
        },
        {
            img: "/assets/images/fredrick.jpg",
            name: "Fredrick Muikia",
            title: "Software Developer",
        },
        {
            img: "/assets/images/kevin.jpg",
            name: "Kevin Gaithe",
            title: "Software Developer",
        }
    ]
}