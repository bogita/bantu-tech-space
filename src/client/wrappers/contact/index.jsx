import React, {Component} from 'react'
import style from './scss/contact.scss'
import request from 'superagent'

export default class Contact extends Component {
    
    constructor(props){
        super(props)
        this.state = {
            name: "",
            business: "",
            phoneNo: "",
            email: "",
            message: ""
        }
    }

    updateData(data){
        this.setState(data)
    }

    closeAlerts(){
        document.getElementById("success-alert").classList.add("hidden")
        document.getElementById("failure-alert").classList.add("hidden")
    }

    clearForm(){
        this.closeAlerts()
        this.setState({
            name: "",
            business: "",
            phoneNo: "",
            email: "",
            message: ""
        })
    }
    
    validData(){
        let check = true;

        document.getElementById("required-name").classList.add("hidden")
        document.getElementById("required-business").classList.add("hidden")
        document.getElementById("required-phone").classList.add("hidden")
        document.getElementById("required-email").classList.add("hidden")
        document.getElementById("required-message").classList.add("hidden")
        
        if(!this.state.phoneNo.match(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/) &&
        !this.state.phoneNo.match(/^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/)){
            document.getElementById("required-phone").classList.remove("hidden")
            check=false;
        }

        if(this.state.name == ""){
            document.getElementById("required-name").classList.remove("hidden")
            check=false;
        }
        
        if(this.state.business == ""){
            document.getElementById("required-business").classList.remove("hidden")
            check=false;
        } 
        
        if(this.state.email == ""){
            document.getElementById("required-email").classList.remove("hidden")
            check=false;
        }

        if(this.state.message == ""){
            document.getElementById("required-message").classList.remove("hidden")
            check=false;
        }

        return check;
    }

    submitForm(){
        if(!document.getElementById("success-alert").classList.contains("hidden")){
            document.getElementById("success-alert").classList.add("hidden")
        }
        if(!document.getElementById("failure-alert").classList.contains("hidden")){
            document.getElementById("failure-alert").classList.add("hidden")
        }
        
        if(this.validData()){
            document.getElementById("submit").classList.add("pageloader")
            request
            .post('/api/mail')
            .send(this.state )
            .set('Accept', 'application/json')
            .end(function (error, res) {
                document.getElementById("submit").classList.remove("pageloader")
                if(res.status != 200){ 
                    document.getElementById("failure-alert").classList.remove("hidden")
                }else{
                    document.getElementById("success-alert").classList.remove("hidden")
                }
            });
        }
    }

    render(){
        return(
            <div className="contact-wrapper">
                <div className="contact-us-form">
                    <div className="field">
                        <label className="label">Name:</label>
                        <div className="field-body">
                            <div className="field">
                            <p className="control is-normal">
                                <input className="input is-rounded is-normal" type="text" value={this.state.name} placeholder="Name"
                                onChange={(e) => {this.updateData({name: e.target.value})}} />
                            </p>
                            </div>
                        </div>
                        <p className="help is-danger hidden" id="required-name">A name is required</p>
                    </div>
                    <div className="field">
                        <label className="label">Business:</label>
                        <div className="field-body">
                            <div className="field">
                            <p className="control is-normal">
                                <input className="input is-rounded is-normal" type="text" value={this.state.business}  placeholder="Company/Business"
                                onChange={(e) => {this.updateData({business: e.target.value})}} />
                            </p>
                            </div>
                        </div>
                        <p className="help is-danger hidden" id="required-business">A business name is required</p>
                    </div>
                    <div className="field">
                        <label className="label">Phone No:</label>
                        <div className="field-body">
                            <div className="field">
                            <p className="control is-normal">
                                <input className="input is-rounded is-normal" type="text" value={this.state.phoneNo}  placeholder="Phone Number (+XX-XXXX-XXXX or XXX-XXX-XXXX)"
                                onChange={(e) => {this.updateData({phoneNo: e.target.value})}} />
                            </p>
                            </div>
                        </div>
                        <p className="help is-danger hidden" id="required-phone">A valid phone number is required</p>
                    </div>
                    <div className="field">
                        <label className="label">Email:</label>
                        <div className="field-body">
                            <div className="field">
                            <p className="control is-normal">
                                <input className="input is-rounded is-normal" type="text" value={this.state.email}  placeholder="Email"
                                onChange={(e) => {this.updateData({email: e.target.value})}}  />
                            </p>
                            </div>
                        </div>
                        <p className="help is-danger hidden" id="required-email">An email is required</p>
                    </div>
                    <div className="field">
                        <label className="label">Message:</label>
                        <div className="field-body">
                            <div className="field">
                            <p className="control is-normal">
                                <textarea className="textarea" rows= "5" cols="50" placeholder="Please give a short description of the solution you are looking for and we will reach out to you as soon as possible!"
                                value={this.state.message} onChange={(e) => {this.updateData({message: e.target.value})}} ></textarea>
                            </p>
                            </div>
                        </div>
                        <p className="help is-danger hidden" id="required-message">A message is required</p>
                    </div>
                    <div>
                        <button id="submit" className="button is-info full-width" onClick={(e) => this.submitForm()}>Submit</button>
                        <div id="success-alert" className="hidden notification is-success">
                            <button className="delete" onClick={(e) => this.clearForm()}></button>
                            <p><strong>Thanks!</strong>your request was successfully processed. We will get back to you asap!</p>
                        </div>
                        <div id="failure-alert" className="hidden notification is-danger">
                            <button className="delete" onClick={(e) => this.clearForm()}></button>
                            <p><strong>Error! </strong>failed to process email due to server error. Try again</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}
