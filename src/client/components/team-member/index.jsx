import React, {Component} from 'react'

export default (props) => {
    
    return(
        <div className="member">
            <img className="member-picture" src={props.img} />
            <p>{props.name}<br />{props.title}</p>
        </div>
    )

}