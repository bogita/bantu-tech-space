import React, {Component} from 'react'

export default (props) => {
    
    return(
        <div className="member">
            <div className="member-header">
                <img className="member-picture" src={props.img} />
                <div className="member-profile">
                    <h3>{props.name}</h3>
                    <p>{props.title}</p>
                </div>
            </div>
            <div className="member-bio">
                <p>{props.bio}</p>
            </div>
        </div>
    )

}