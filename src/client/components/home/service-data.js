export default {
    services: [
        {
            img: "/assets/images/web-design.jpeg",
            img_md: "/assets/images/web-design-md.jpeg",
            img_sm: "/assets/images/web-design-sm.jpeg",
            title: "Websites Development",
            desc: "From your ecommerce site, blog or business portal."
            // desc: "Web applications essential to modern day business and can be the difference between growing your profit margins or going out of business. Our goal as a team is to assist you in developing elegant applications that are user and mobile friendly."
        },
        {
            img: "/assets/images/mobile-apps.jpeg",
            img_md: "/assets/images/mobile-apps-md.jpeg",
            img_sm: "/assets/images/mobile-apps-sm.jpeg",
            title: "Mobile Applications Development",
            desc: "Building for both IOS & Android."
            // desc: "A mobile application for your business means having a consistent user base that you are always connected to. This connection is important as it grants your business direct connection with its core customers, who play a key role in driving growth. Therefore, we strive to build stable applications that keep user base engaged."
        },
        // {
        //     img: "/assets/images/security-img.jpeg",
        //     img_md: "/assets/images/security-img-md.jpeg",
        //     img_sm: "/assets/images/security-img-sm.jpeg",
        //     title: "Security Analysis",
        //     desc: "Data is the oil that powers our modern applications and as we continue produce more, it is becoming increasingly vital that our data is protected from outside threats."
        // },
        // {
        //     img: "/assets/images/system-optimize.jpeg",
        //     img_md: "/assets/images/system-optimize-md.jpeg",
        //     img_sm: "/assets/images/system-optimize-sm.jpeg",
        //     title: "System Optimizations",
        //     desc: "It is our view that at times, building from ground up may not always be the optimal choice which is why some may prefer to optimize their systems/legacy code to work for me modern world rather than a complete rebuild which can be costly."
        // },
        {
            img: "/assets/images/data.jpeg",
            img_md: "/assets/images/data-md.jpeg",
            img_sm: "/assets/images/data-sm.jpeg",
            title: "Data Management",
            desc: "Organizing, structuring and storing data. Additionally, we provide tools for analyzing and generating reports from the data."
            // desc: "Unorganized data is hard to maintain and likely fails to provide its true value. We assist our customers with securing and structuring their data in a manner that is easy to maintain and grow."
        },
        {
            img: "/assets/images/system-optimize.jpeg",
            img_md: "/assets/images/system-optimize-md.jpeg",
            img_sm: "/assets/images/system-optimize-sm.jpeg",
            title: "Business based solutions",
            desc: "Analyzing the problems and providing the applicable solutions"
            // desc: "Unorganized data is hard to maintain and likely fails to provide its true value. We assist our customers with securing and structuring their data in a manner that is easy to maintain and grow."
        }
    ]
}