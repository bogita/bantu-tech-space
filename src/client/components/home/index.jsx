import React, {Component} from 'react'
import Style from './scss/home.scss'
import data from './service-data'

export default (props) => {
    
    const services = data.services.map((elem, i) => {
        const pullClass = i % 2 == 0 ? "is-light" : "is-info" 
        const space = i == 2? <div className="empty-space"></div> : <span className="hidden"></span>
        return (
            <div key={i}>
                {space}
                <div className={`${pullClass} columns service-content`}>
                    <div className="column left-content is-half">
                        <img className="service-img"  
                        srcSet={
                            `${elem.img_md} 300w,
                            ${elem.img_sd} 600w,
                            ${elem.img} 900w` 
                        
                        } />
                    </div>
                    <div className="column right-content is-half">
                        <h3 className="desc-title">{elem.title}</h3>
                        <p>{elem.desc}</p>
                    </div>
                </div>
            </div>
        )
    });

    return(
        <div id="home-page-wrapper">
            <div id="home-page-content">
                <section className="hero is-dark">
                    <div className="hero-body">
                        <div className="container">
                        <h1 className="title">
                            Hiqohn Tech
                        </h1>
                        <h2 className="subtitle">
                            Innovating our way through challenges of the modern world
                        </h2>
                        </div>
                    </div>
                </section>
                <div id="shape-wrapper">
                    <div id="triagle"></div>
                </div>
                {services}
            </div>
        </div>
    )

}