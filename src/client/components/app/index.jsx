import React, {Component} from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from '../home'
import About from '../about-us'
import Contact from '../../wrappers/contact'
import Team from '../../wrappers/team'
import Header from '../header'
import Footer from '../footer'

import style from './scss/app.scss'

export default class App extends Component {
    
    constructor(props){
        super(props)
    }

    render(){
        return(
            <div id="app-wrapper">
                <Header/>
                <Switch>
                    <Route exact path="/" render={(props) => (<Home {...props} />)} />
                    <Route path="/about-us" render={(props) => (<About {...props} />) }/>
                    <Route path="/team" render={(props) => (<Team {...props} />)} />
                    <Route path="/contact" render={(props) => (<Contact {...props} />) } />
                </Switch>
                <Footer />
            </div>
        )
    }

}
