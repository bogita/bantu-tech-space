import React, {Component} from 'react';
import {NavLink} from 'react-router-dom'

export default (props) => {
    
    return(
        <footer className="footer">
            <div className="content has-text-centered">
                <NavLink target="_blank" to="https://www.facebook.com/hiqohntech/"> <i className="fab fa-facebook-f"></i> </NavLink>
                <NavLink target="_blank" to="https://www.twitter.com/hiqohntech"> <i className="fab fa-twitter"></i> </NavLink>
                <NavLink target="_blank" to="https://www.instagram.com/hiqohntech"> <i className="fab fa-instagram"></i> </NavLink>
                <p>cs@hiqohntech.com</p>
                <p>&copy; 2018 hiqohntech.com</p>
            </div>
        </footer>
    )

}