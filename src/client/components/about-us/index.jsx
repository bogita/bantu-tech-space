import React, {Component} from 'react'
import Style from './scss/about-us.scss'

export default (props) => {

    return(
        <div className="about-us-wrapper">
            <div className="about-us">
                <p>
                The idea of Hiqohn tech originated from our three founders John, Chris and Hiram. 
                It started form a desire to build an agile software development team that 
                is capable on tackling any tech related project and deliverying a product of the highest quality. 
                It was first put into action on January 6, 2018 as a three member team and since  
                grown to seven members, all whom have multiple years of experience and education as software developers. 
                Based in St. Louis, Missouri, Hiqohn tech focuses on building web and mobile applications 
                and also assisting our clients with data managment. 
                </p>
            </div>
        </div>
    )

}