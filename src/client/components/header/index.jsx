import React, {Component} from 'react'
import {NavLink} from 'react-router-dom'

export default (props) => {
    
    return(
        <nav className="navbar " role="navigation" aria-label="main navigation">
            <div className="container">
                <div className="navbar-brand">
                    <NavLink to='/' className="navbar-item">
                        <img src="/assets/images/hqt/svg/color_logo_transparent.svg" alt="Hiqohn Tech Home Page" />
                    </NavLink>

                    <a role="button" className="navbar-burger"  data-target="navMenu" aria-label="menu" aria-expanded="false">
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                    </a>
                </div>
                <div className="navbar-menu" id="navMenu">
                    <div className="navbar-end">
                        <NavLink to='/about-us' className="navbar-item" >About Us</NavLink>
                        <NavLink to='/team' className="navbar-item" >Team</NavLink>
                        <NavLink to='/contact' className="navbar-item" >Contact</NavLink>
                        <a className="navbar-item" href="https://blog.hiqohntech.com" target="_blank" >Blog</a>
                    </div>
                </div>
            </div>
        </nav>
    )

}