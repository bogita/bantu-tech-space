import express from 'express'
import bodyParser from 'body-parser'
import React from 'react'
import { renderToString } from 'react-dom/server'
import {StaticRouter} from 'react-router-dom'
import App from '../client/components/app/index'
import template from './template'
import logger from './config/logger'
import mailler from './mail/index'

const path = require('path')

const APP_PORT=9890;
const app = express()


app.use(bodyParser.json())

app.use('/assets/images', express.static(path.resolve(__dirname,'../public/images/')) );
app.use('/assets/css', express.static(path.resolve(__dirname,'../../dist/')) );
app.use('/assets/js', express.static(path.resolve(__dirname,'../../dist/')) );

app.post('/api/mail', (req, res)=>{
    logger.debug("Processing mail request: \n"+req.body)
    mailler(req.body, res)    
});

app.get('*', (req, res) => {
    logger.info("server side rendering application! \n", {reqBody: req.body, reqUrl: req.url} );
    
    const context = {}  
    const appString = renderToString(
        <StaticRouter location={req.url} context={context}>
            <App />
        </StaticRouter>
    );

    res.send(template({
      body: appString,
      title: 'Hiqohn Tech'
    }));
});

app.listen(APP_PORT, () => {
    logger.info(`************Application started on port ${APP_PORT}************`)
})