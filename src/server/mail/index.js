import mailer from 'nodemailer'
import logger from '../config/logger'

export default (data, res) => {
    mailer.createTestAccount((err, account) => {

        // create reusable transporter object using the default SMTP transport
        let transporter = mailer.createTransport({
            pool: true,
            host: 'smtp.gmail.com',
            port: 587,
            secure: false,
            auth: {
                user: "hiqohntech@gmail.com",
                pass: "Muigai89" 
            },
            tls:{
                rejectUnauthorized: false
            }
        });
    
        transporter.verify(function(error, success) {
            if (error) { logger.debug("Server not ready to process mail!") } 
            else { logger.info('Server is ready to take our messages') }
         });

        // setup email data with unicode symbols
        let mailOptions = {
            from: `${data.name} <${data.email}>`,
            to: 'cs@hiqohntech.com',
            subject: `${data.name} from ${data.business} - Hiqohn Contact Request`,
            html: `<div style="width: 500px;">
                        <div style="text-align: justify; text-indent: 50px;">${data.message}</div>
                        <br/><br/><br/>
                        <div>
                            <p><strong>Name:</strong> ${data.name}</p>
                            <p><strong>Phone Number:</strong> ${data.phoneNo}</p>
                            <p><strong>Email::</strong> ${data.email}</p>
                        </div>
                    </div>`
        };

        logger.debug("Sending: \n", mailOptions)
    
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                logger.debug("Failed to send mail.", {info: info, emailData: data})
                logger.debug(error);
                res.status(500)
                res.send("Failed to process mail request.")
                transporter.close()
                return null;
            }
            res.status(200)
            res.send("Successfully processed mail request!")
            logger.info('Message sent: %s', info.messageId)
            transporter.close()
        })

    });

}