export default ({ body, title }) => {
    return `
    <!doctype html>
<html lang="en">
  <head>
    <title>Hiqohn Tech</title>
    <meta charSet="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=2.5, user-scalable=1" name="viewport"/>
    <link rel="shortcut icon" type="image/x-icon" href="/assets/images/hqt/svg/color_logo_transparent.svg" />
    <link href="https://fonts.googleapis.com/css?family=Raleway:500" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma-timeline@3.0.0/dist/css/bulma-timeline.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma-pageloader@2.1.0/dist/css/bulma-pageloader.min.css">
    <link href="/assets/css/style.css" rel="stylesheet" />
    <script defer src="https://use.fontawesome.com/releases/v5.1.0/js/all.js"></script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114649967-1"></script>
    
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-114649967-1');
    </script>
    <script type="application/ld+json">
      {
        "@context": "http://schema.org",
        "@type": "Organization",
        "name" : "Hiqohn Tech",
        "url": "http://hiqohntech.com",
        "sameAs" : [
        "https://www.facebook.com/hiqohntech/",
        "",
        "",
        ""
        ]
      }
    </script>
    <script type="application/ld+json">
      {
        "@context": "http://schema.org",
        "@type": "LocalBusiness",
        "name" : "Hiqohn Tech",
        "url": "http://hiqohntech.com",
        "logo": "http://res.cloudinary.com/hiqohntech/image/upload/v1522526415/logo.png",
        "image": "",
        "description": "Hiqohn Tech is a small agile software development team that specializes in developing web and mobile applications, organizing customer data and network optimizations.",
        "telephone": "",
        "address": {
        "@type": "PostalAddress",
        "addressLocality": "Hazelwood",
        "addressRegion": "Mo",
        "streetAddress": "",
        "postalCode": "63042"
        },
        "openingHours": [
        "Mon - Sun 01:00 - 24:00"
        ]
      }
    </script>
  </head>
  <body>
      <div  id="root">
      ${body}
      </div>
      <script type="text/javascript" src="/assets/js/bundle.js"></script>
  </body>
  <script>
    document.addEventListener('DOMContentLoaded', () => {
      const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

      if ($navbarBurgers.length > 0) {
        $navbarBurgers.forEach( el => {

          el.addEventListener('click', () => {
            const target = el.dataset.target;
            const $target = document.getElementById(target);

            el.classList.toggle('is-active');
            $target.classList.toggle('is-active');
          });

        });
      }

    });
  </script>
</html>

    `;
  };