import log4js from'log4js'

log4js.configure({
  appenders: {
    out: { type: 'stdout' },
    debug: { 
      type: 'file', 
      filename: 'logs/bantu-tech-space-debug.log',
      pattern: '-yyyy-MM-dd',
      alwaysIncludePattern : true,
      maxLogSize: 20480 
    },
    info: { 
      type: 'file', 
      filename: 'logs/bantu-tech-space-info.log',
      pattern: '-yyyy-MM-dd',
      alwaysIncludePattern : true,
      maxLogSize: 20480 
    }
  },
  categories: {
    default: { appenders: [ 'out', 'debug' ], level: 'debug' },
    info: { appenders: [ 'out', 'info' ], level: 'info' }
  }
});


export default log4js.getLogger()