FROM node:8.9.4


RUN mkdir ~/bantu-tech-space

WORKDIR ~/bantu-tech-space

RUN apt-get update

RUN npm install -g yarn

RUN npm install pm2@latest -g

RUN pm2 update

COPY package*.json ./
COPY yarn.lock ./

RUN yarn install

COPY . .

EXPOSE 9890

#CMD ["pm2-runtime",  "process.yml"]
CMD ["pm2", "start", "process.yml", "--no-daemon"]